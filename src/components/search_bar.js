import React from 'react';

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = { term: '' }
    }

    handleInputChange = (event) => {
        const { value } = event.target;
        
        this.setState({ term: value })
        this.props.onSearchTermChange(value)
    };

    render() {
        return (
            <div className='search-bar'>
                <input value={this.state.term} onChange={this.handleInputChange} />
            </div>
        )
    }
}

export default SearchBar;
